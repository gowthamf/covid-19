import 'dart:async';
import 'package:corona_details/checkinternetconnection.dart';
import 'package:corona_details/models/country.dart';
import 'package:corona_details/services.dart';
import 'package:flutter/material.dart';

class CountryList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CountryList();
  }
}

class _CountryList extends State<CountryList> {
  Stream<List<Country>> _countryList;
  StreamSubscription _connectionChangeStream;
  TextEditingController _controller = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    CheckInternetConnection checkInternetConnection =
        CheckInternetConnection.getInstance();
    _connectionChangeStream = checkInternetConnection.connectionChange
        .listen((onData) => {connectionChanged(onData)});
  }

  void connectionChanged(Connections connection) {
    if (connection.connectionState) {
      setState(() {
        _countryList = Services.fetchCountryDetails().asStream();
      });
    }
  }

  @override
  void dispose() {
    _connectionChangeStream.cancel();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Form(
            key: _formKey,
            child: TextFormField(
              textInputAction: TextInputAction.done,
              onChanged: (value) {
                filterbyCountry(value);
              },
              controller: _controller,
              decoration: InputDecoration(
                  hintText: "Search Country",
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.0)))),
            ),
          ),
        ),
        Expanded(
          child: StreamBuilder<List<Country>>(
            builder:
                (BuildContext context, AsyncSnapshot<List<Country>> snapshot) {
              if (snapshot.hasData) {
                return RefreshIndicator(
                    child: ListView.separated(
                      physics: AlwaysScrollableScrollPhysics(),
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                          child: ListTile(
                            title: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(bottom: 10),
                                    child: Text(
                                      '${index + 1}. ${snapshot.data[index].countryName}',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18),
                                    ),
                                  ),
                                  Wrap(
                                    direction: Axis.horizontal,
                                    alignment: WrapAlignment.spaceBetween,
                                    runSpacing: 25,
                                    spacing: 25,
                                    children: <Widget>[
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Wrap(
                                            direction: Axis.vertical,
                                            alignment:
                                                WrapAlignment.spaceBetween,
                                            runSpacing: 25,
                                            spacing: 25,
                                            children: <Widget>[
                                              Text(
                                                  'Total Cases : ${snapshot.data[index].totalCases}',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              Text(
                                                  'New Cases : +${snapshot.data[index].todayCases}',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color:
                                                          Color(0xFFEF6C00))),
                                            ],
                                          )
                                        ],
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Wrap(
                                            direction: Axis.vertical,
                                            alignment:
                                                WrapAlignment.spaceBetween,
                                            runSpacing: 25,
                                            spacing: 25,
                                            children: <Widget>[
                                              Text(
                                                  'Total Deaths : ${snapshot.data[index].totalDeaths}',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              Text(
                                                  'New Deaths : +${snapshot.data[index].todayDeaths}',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.red)),
                                            ],
                                          ),
                                        ],
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Wrap(
                                            direction: Axis.vertical,
                                            alignment:
                                                WrapAlignment.spaceBetween,
                                            runSpacing: 25,
                                            spacing: 25,
                                            children: <Widget>[
                                              Text(
                                                  'Total Recovered : ${snapshot.data[index].totalRecovered}',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              Text(
                                                  'Serious, Critical : ${snapshot.data[index].totalCritical}',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold)),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                      shrinkWrap: true,
                      itemCount: snapshot.data.length,
                      separatorBuilder: (context, index) => const Divider(),
                    ),
                    onRefresh: () {
                      return Future.delayed(
                        const Duration(seconds: 5),
                        () {
                          setState(() {
                            _countryList =
                                Services.fetchCountryDetails().asStream();
                          });
                        },
                      );
                    });
              }
              return Center(
                child: CircularProgressIndicator(
                  strokeWidth: 6.0,
                  backgroundColor: Theme.of(context).primaryColor,
                ),
              );
            },
            stream: _countryList,
          ),
        ),
      ],
    ));
  }

  void filterbyCountry(String value) {
    setState(() {
      _countryList = Services.searchCountry(value).asStream();
    });
  }
}
