import 'dart:convert';

import 'package:corona_details/models/country.dart';
import 'package:corona_details/models/summarydetails.dart';
import 'package:http/http.dart' as http;

class Services {
  static List<Country> countryList = List<Country>();

  static Future<List<Country>> fetchCountryDetails() async {
    final response = await http.get('https://corona.lmao.ninja/countries');
    final List<Country> _countryList = List<Country>();

    if (response.statusCode == 200) {
      List<dynamic> countryLists = json.decode(response.body);
      _countryList
          .addAll(countryLists.map((f) => Country.fromJson(f)).toList());
      countryList.clear();
      countryList.addAll(_countryList);
      return _countryList;
    } else {
      throw Exception('Failed to load Data');
    }
  }

  static Future<SummaryDetails> fetchSummaryDetails() async {
    final response = await http.get('https://corona.lmao.ninja/all');
    SummaryDetails _summaryDetails = SummaryDetails();

    if (response.statusCode == 200) {
      dynamic summaryDetails = json.decode(response.body);
      _summaryDetails = SummaryDetails.fromJson(summaryDetails);
      return _summaryDetails;
    } else {
      throw Exception('Failed to load Data');
    }
  }

  static Future<List<Country>> searchCountry(String countryName) async {
    if (countryName.isNotEmpty) {
      var filteredList = countryList
          .where((c) =>
              c.countryName.toLowerCase().startsWith(countryName.toLowerCase()))
          .toList();
      return filteredList;
    }
    return countryList;
  }

  static Future<List<Country>> sortByTotalCase() async {
    if (countryList.length > 0) {
      var sortedList = List<Country>.from(countryList);
      sortedList.sort((a, b) => a.totalCases.compareTo(b.totalCases));
      return sortedList;
    }
    return [];
  }

  static Future<List<Country>> sortByTotalDeath() async {
    if (countryList.length > 0) {
      var sortedList = List<Country>.from(countryList);
      sortedList.sort((a, b) => a.totalDeaths.compareTo(b.totalDeaths));
      return sortedList;
    }
    return [];
  }

  static Future<List<Country>> sortByTodaysCase() async {
    if (countryList.length > 0) {
      var sortedList = List<Country>.from(countryList);
      sortedList.sort((a, b) => a.todayCases.compareTo(b.todayCases));
      return sortedList;
    }
    return [];
  }

  static Future<List<Country>> sortByTodaysDeath() async {
    if (countryList.length > 0) {
      var sortedList = List<Country>.from(countryList);
      sortedList.sort((a, b) => a.todayDeaths.compareTo(b.totalDeaths));
      return sortedList;
    }
    return [];
  }

  static Future<List<Country>> sortByTotalRecovered() async {
    if (countryList.length > 0) {
      var sortedList = List<Country>.from(countryList);
      sortedList.sort((a, b) => a.totalRecovered.compareTo(b.totalRecovered));
      return sortedList;
    }
    return [];
  }
}
