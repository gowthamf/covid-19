import 'package:corona_details/indicator.dart';
import 'package:corona_details/models/summarydetails.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class StatsChart extends StatefulWidget {
  final SummaryDetails summaryDetails;

  StatsChart({this.summaryDetails});

  @override
  State<StatefulWidget> createState() {
    return _StatsChart();
  }
}

class _StatsChart extends State<StatsChart> {
  @override
  Widget build(BuildContext context) {
    return Wrap(
      alignment: WrapAlignment.start,
      direction: Axis.vertical,
      children: <Widget>[
        Row(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Wrap(
                  alignment: WrapAlignment.start,
                  direction: Axis.vertical,
                  children: <Widget>[
                    Indicator(
                      color: Colors.green,
                      text: 'Recovered',
                      isSquare: true,
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Indicator(
                      color: Colors.red,
                      text: 'Deaths',
                      isSquare: true,
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Indicator(
                      color: Colors.blue,
                      text: 'Active Cases',
                      isSquare: true,
                    ),
                  ],
                )
              ],
            ),
            PieChart(
              PieChartData(
                  pieTouchData: PieTouchData(),
                  borderData: FlBorderData(
                    show: false,
                  ),
                  sectionsSpace: 0,
                  centerSpaceRadius: 50,
                  sections: showingSections()),
            ),
          ],
        )
      ],
    );
  }

  List<PieChartSectionData> showingSections() {
    double percentageDeaths = widget.summaryDetails.totalDeaths /
        widget.summaryDetails.totalCases *
        100;
    double percentageRecovered = widget.summaryDetails.totalRecovered /
        widget.summaryDetails.totalCases *
        100;
    int activeCases = (widget.summaryDetails.totalCases -
        widget.summaryDetails.totalDeaths -
        widget.summaryDetails.totalRecovered);
    double percentageActiveCases =
        activeCases / widget.summaryDetails.totalCases * 100;
    return List.generate(3, (i) {
      return PieChartSectionData(
        color: i == 0 ? Colors.green : i == 1 ? Colors.red : Colors.blue,
        value: i == 0
            ? percentageRecovered.roundToDouble()
            : i == 1
                ? percentageDeaths.roundToDouble()
                : percentageActiveCases.roundToDouble(),
        title: i == 0
            ? '${percentageRecovered.round()}%'
            : i == 1
                ? '${percentageDeaths.round()}%'
                : '${percentageActiveCases.round()}%',
        titleStyle: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.bold,
            color: const Color(0xffffffff)),
      );
    });
  }
}
