import 'dart:convert';

import 'package:flutter/material.dart';

class TwitterSelectableChips extends StatefulWidget
    implements PreferredSizeWidget {
  Function selectChip;

  static String watchDogEmbeded = '''<!DOCTYPE html><html>
<head><title>Navigation Delegate Example</title></head>
<body>
<a class="twitter-timeline" href="https://twitter.com/TeamWatchDog?ref_src=twsrc%5Etfw">Tweets by TeamWatchDog</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</body>
</html>
''';

  static String whoEmbeded = '''<!DOCTYPE html><html>
<head><title>Navigation Delegate Example</title></head>
<body>
<a class="twitter-timeline" href="https://twitter.com/WHO?ref_src=twsrc%5Etfw">Tweets by WHO</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</body>
</html>''';

  TwitterSelectableChips({this.selectChip});

  List<TwitterChips> _twitterChipsList = <TwitterChips>[
    TwitterChips(
        displayName: 'TeamWatchDog',
        twitterUrl: watchDogEmbeded,
        isEmbeded: true),
    TwitterChips(
        displayName: 'coronavirus',
        twitterUrl: 'https://twitter.com/hashtag/CoronaVirus',
        isEmbeded: false),
    TwitterChips(
        displayName: 'CoronavirusOutbreak',
        twitterUrl: 'https://twitter.com/hashtag/CoronavirusOutbreak',
        isEmbeded: false),
    TwitterChips(
        displayName: '#COVID19',
        twitterUrl: 'https://twitter.com/hashtag/COVID19',
        isEmbeded: false),
    TwitterChips(displayName: 'WHO', twitterUrl: whoEmbeded, isEmbeded: true),
  ];

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TwitterSelectableChips();
  }
}

class _TwitterSelectableChips extends State<TwitterSelectableChips> {
  int _value = 1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Wrap(
          spacing: 10,
          children: List<Widget>.generate(
            widget._twitterChipsList.length,
            (int index) {
              return FilterChip(
                  selected: _value == index,
                  label: Text(widget._twitterChipsList[index].displayName),
                  onSelected: (selected) async {
                    setState(() {
                      _value = selected ? index : null;
                      if (widget._twitterChipsList[index].isEmbeded) {
                        final String contentBase64 = base64Encode(
                          const Utf8Encoder().convert(
                              widget._twitterChipsList[index].twitterUrl),
                        );
                        String _embededUrl =
                            'data:text/html;base64,$contentBase64';
                        widget.selectChip(_embededUrl);
                      } else {
                        widget.selectChip(
                            widget._twitterChipsList[index].twitterUrl);
                      }
                    });
                  });
            },
          ),
        ));
  }
}

class TwitterChips {
  final String displayName;
  final String twitterUrl;
  final bool isEmbeded;

  TwitterChips({this.displayName, this.twitterUrl, this.isEmbeded});
}
