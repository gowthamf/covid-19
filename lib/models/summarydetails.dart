class SummaryDetails {
  final int totalcases;
  final int totaldeaths;
  final int totalrecovered;

  SummaryDetails({this.totalcases, this.totaldeaths, this.totalrecovered});

  int get totalCases => totalcases;
  int get totalDeaths => totaldeaths;
  int get totalRecovered => totalrecovered;

  factory SummaryDetails.fromJson(Map<String, dynamic> json) {
    return SummaryDetails(
        totalcases: json['cases'],
        totaldeaths: json['deaths'],
        totalrecovered: json['recovered']);
  }
}
