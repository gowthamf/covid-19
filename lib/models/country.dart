class Country {
  final String countryname;
  final int totalcases;
  final int todaycases;
  final int totaldeaths;
  final int todaydeaths;
  final int totalrecovered;
  final int totalcritical;

  Country(
      {this.countryname,
      this.totalcases,
      this.todaycases,
      this.todaydeaths,
      this.totaldeaths,
      this.totalrecovered,
      this.totalcritical});

  String get countryName => countryname;
  int get totalCases => totalcases;
  int get todayCases => todaycases;
  int get totalDeaths => totaldeaths;
  int get totalRecovered => totalrecovered;
  int get totalCritical => totalcritical;
  int get todayDeaths => todaydeaths;

  factory Country.fromJson(Map<String, dynamic> json) {
    return Country(
        countryname: json['country'],
        totalcases: json['cases'],
        totalcritical: json['critical'],
        totaldeaths: json['deaths'],
        todaycases: json['todayCases'],
        todaydeaths: json['todayDeaths'],
        totalrecovered: json['recovered']);
  }
}
