import 'dart:async';
import 'dart:io';

import 'package:corona_details/checkinternetconnection.dart';
import 'package:corona_details/country_list.dart';
import 'package:corona_details/summary.dart';
import 'package:corona_details/tabsNavigation.dart';
import 'package:corona_details/twitter_updates.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

import 'twitter_updates.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Home();
  }
}

class _Home extends State<Home> with SingleTickerProviderStateMixin {
  TabController _tabController;
  StreamSubscription _connectionChangeStream;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool isOffline = false;

  final List<Tab> _tabs = <Tab>[
    Tab(
      text: 'Summary',
    ),
    Tab(
      text: 'By Country',
    ),
  ];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: _tabs.length, vsync: this);
    _tabController.addListener(() {
      if (_tabController.indexIsChanging) {
        FocusScope.of(context).requestFocus(FocusNode());
      }
    });
    CheckInternetConnection checkInternetConnection =
        CheckInternetConnection.getInstance();
    _connectionChangeStream =
        checkInternetConnection.connectionChange.listen((onData) {
      connectionChanged(onData);
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    _connectionChangeStream.cancel();

    super.dispose();
  }

  void connectionChanged(Connections connection) {
    String _message = '';
    if (!connection.isInitialConnection || !connection.connectionState) {
      if (!connection.connectionState && connection.isNotify) {
        _message = 'No Internet Connection!';
        final snackBar = SnackBar(
          content: Text(_message),
          duration: Duration(seconds: 2),
        );
        setState(() {
          _scaffoldKey.currentState.showSnackBar(snackBar);
        });
      } else if (connection.connectionState && connection.isNotify) {
        _message = 'Internet Connection is Available';
        final snackBar = SnackBar(
          content: Text(_message),
          duration: Duration(seconds: 2),
        );
        setState(() {
          _scaffoldKey.currentState.showSnackBar(snackBar);
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.symmetric(vertical: 5),
                child: RaisedButton(
                  color: Colors.white,
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => TwitterUpdates()));
                  },
                  shape: CircleBorder(),
                  child: Image.asset(
                    'assets/twitter.png',
                    width: 25,
                  ),
                ))
          ],
          title: Text('COVID- 19 Update', style: GoogleFonts.lato()),
          bottom: TabNavigation(
            tabController: _tabController,
            tabs: _tabs,
          ),
        ),
        body: TabBarView(
          children: [Summary(), CountryList()],
          controller: _tabController,
        ),
      ),
    );
  }
}
