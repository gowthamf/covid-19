import 'package:corona_details/checkinternetconnection.dart';
import 'package:corona_details/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(Main());
  CheckInternetConnection checkInternetConnection =
      CheckInternetConnection.getInstance();
  checkInternetConnection.initialize();
}

class Main extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
              create: (context) => CheckInternetConnection.getInstance())
        ],
        child: MaterialApp(
          theme: ThemeData(
              brightness: Brightness.light,
              primaryColor: Colors.blue[800],
              accentColor: Colors.blue[500],
              textTheme: GoogleFonts.exoTextTheme(
                Theme.of(context).textTheme,
              ),
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              tabBarTheme: TabBarTheme(
                  indicatorSize: TabBarIndicatorSize.tab,
                  labelColor: Color(0xFFE3F2FD),
                  indicator: UnderlineTabIndicator(
                      borderSide:
                          BorderSide(width: 3, color: Color(0xFF42A5F5)))),
              buttonTheme: ButtonThemeData(buttonColor: Color(0xFF536DFE))),
          debugShowCheckedModeBanner: false,
          home: Home(),
        ));
  }
}
