import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/rxdart.dart';

class CheckInternetConnection extends ChangeNotifier {
  static final CheckInternetConnection _checkInternetConnection =
      CheckInternetConnection._internal();
  CheckInternetConnection._internal();

  static CheckInternetConnection getInstance() => _checkInternetConnection;

  BehaviorSubject<Connections> connectionChangeController =
      BehaviorSubject<Connections>();

  final Connectivity _connectivity = Connectivity();

  bool isInitialize;
  bool hasConnection = false;

  void initialize() {
    isInitialize = true;
    _connectivity.onConnectivityChanged.listen(_connectionChange);
  }

  Stream get connectionChange => connectionChangeController.stream;

  void dispose() {
    connectionChangeController.close();
  }

  void _connectionChange(ConnectivityResult result) {
    if (result == ConnectivityResult.none) {
      if (isInitialize) {
        connectionChangeController.sink.add(Connections(
            connectionstate: false, isnotify: true, isinitialconnection: true));
      } else {
        connectionChangeController.sink.add(Connections(
            connectionstate: false,
            isnotify: true,
            isinitialconnection: false));
      }

      hasConnection = false;
      // notifyListeners();
    } else {
      if (isInitialize) {
        connectionChangeController.sink.add(Connections(
            connectionstate: true, isinitialconnection: true, isnotify: false));
      } else {
        connectionChangeController.sink.add(Connections(
            connectionstate: true, isinitialconnection: false, isnotify: true));
      }

      hasConnection = true;
      // notifyListeners();
    }

    isInitialize = false;
  }
}

class Connections {
  final bool connectionstate;
  final bool isinitialconnection;
  final bool isnotify;

  Connections({this.connectionstate, this.isinitialconnection, this.isnotify});

  bool get connectionState => connectionstate;
  bool get isInitialConnection => isinitialconnection;
  bool get isNotify => isnotify;
}
