import 'package:flutter/material.dart';

class TabNavigation extends StatelessWidget implements PreferredSizeWidget {
  final TabController tabController;
  final List<Tab> tabs;

  TabNavigation({this.tabController, this.tabs});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return TabBar(
      isScrollable: true,
      tabs: tabs,
      controller: tabController,
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
