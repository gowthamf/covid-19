import 'dart:async';

import 'package:corona_details/checkinternetconnection.dart';
import 'package:corona_details/models/summarydetails.dart';
import 'package:corona_details/services.dart';
import 'package:corona_details/statschart.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Summary extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Summary();
  }
}

class _Summary extends State<Summary> {
  Stream<SummaryDetails> _summaryDetails;
  StreamSubscription _connectionChangeStream;

  @override
  void initState() {
    super.initState();
    CheckInternetConnection checkInternetConnection =
        CheckInternetConnection.getInstance();
    if (checkInternetConnection.hasConnection) {
      _summaryDetails = Services.fetchSummaryDetails().asStream();
    }

    _connectionChangeStream =
        checkInternetConnection.connectionChange.listen((onData) => {
              connectionChanged(onData),
            });
  }

  void connectionChanged(Connections connection) {
    if (connection.connectionState) {
      setState(() {
        _summaryDetails = Services.fetchSummaryDetails().asStream();
      });
    }
  }

  @override
  void dispose() {
    _connectionChangeStream.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.expand(),
      child: Container(
          child: StreamBuilder<SummaryDetails>(
        stream: _summaryDetails,
        builder:
            (BuildContext context, AsyncSnapshot<SummaryDetails> snapshot) {
          if (snapshot.hasData) {
            return Container(
              child: Card(
                child: RefreshIndicator(
                  onRefresh: () {
                    return Future.delayed(
                      const Duration(seconds: 5),
                      () {
                        setState(
                          () {
                            _summaryDetails =
                                Services.fetchSummaryDetails().asStream();
                          },
                        );
                      },
                    );
                  },
                  color: Colors.blue[800],
                  displacement: 45,
                  child: SingleChildScrollView(
                    physics: AlwaysScrollableScrollPhysics(),
                    child: Wrap(
                      spacing: 20,
                      direction: Axis.vertical,
                      children: <Widget>[
                        Text(
                          'Total Cases : ${snapshot.data.totalCases}',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 30),
                        ),
                        Text('Total Deaths : ${snapshot.data.totalDeaths}',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 30,
                                color: Colors.red)),
                        Text(
                            'Total Recovered : ${snapshot.data.totalRecovered}',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 30,
                                color: Colors.green[600])),
                        Container(
                          child: StatsChart(
                            summaryDetails: snapshot.data,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
          return Center(
            child: CircularProgressIndicator(
              strokeWidth: 6.0,
              backgroundColor: Theme.of(context).primaryColor,
            ),
          );
        },
      )),
    );
  }
}
