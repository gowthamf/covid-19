import 'dart:async';
import 'dart:convert';

import 'package:corona_details/twitter_selectable_chips.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TwitterUpdates extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TwitterUpdates();
  }
}

class _TwitterUpdates extends State<TwitterUpdates> {
  WebViewController _controller;
  String selectedTopic = '';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Twitter Updates'),
          bottom: TwitterSelectableChips(selectChip: selectTwitterChips),
        ),
        body: Container(
          child: WebView(
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController c) {
              _controller = c;
            },
            initialUrl: 'https://twitter.com/hashtag/CoronaVirus',
          ),
        ),
      ),
    );
  }

  void selectTwitterChips(String searchContent) async {
    if (_controller != null) {
      await _controller.clearCache();
    }
    await _controller.loadUrl(searchContent);
  }
}
